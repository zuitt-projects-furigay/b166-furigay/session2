const chai = require('chai');
const expect = chai.expect;
const assert = chai.assert
// const { expect } = require("chai")

const http = require('chai-http');
chai.use(http);

describe("API Test Suite", () => {
	it("Test API GET People are running", () => {
		chai.request('http://localhost:5001').get('/people')
		.end((err, res) => {
			expect(res).to.not.equal(undefined);
		})
	})

	it("Test API GET people returns 200", (done) => {
		chai.request('http://localhost:5001')
		.get('/people')
		.end((err, res) => {
			expect(res.status).to.equal(200);
			done();
		})
	})

	it("Test API POST Person returns 400 if no person name", (done) => {
		chai.request('http://localhost:5001')
		.post('/person')
		.type('json')
		.send({
			alias: "Jason",
			age: 28
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		})
	})

	// ACTIVITY
 
	it("[1] Check if person endpoint is running", (done) => {
	    chai.request("http://localhost:5001")
	    .post("/person")
	    .type("json")
	    .send({
	        name: "John Doe",
	        alias: "john",
	        age: 25
	    })
	    .end((err, res) => {
	        assert.equal(res.status, 201);
	        done();
	    })
	});


	it('[2] Test API POST person returns 400 if no ALIAS', (done) => {
		chai.request('http://localhost:5001')
		.post('/person')
		.type('json')
		.send({
			name : "Paulo",
			age: 21
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done();
		})
	}) 

	it('[3] Test API POST person returns 400 if no AGE', (done) => {
		chai.request('http://localhost:5001')
		.post('/person')
		.type('json')
		.send({
			name : "Adriann Paulo",
			alias: "AP"
		})
		.end((err, res) => {
			assert.equal(res.status, 400);
			done()
		})
	})
})







//installing mocha globaly
// npm install -g mocha